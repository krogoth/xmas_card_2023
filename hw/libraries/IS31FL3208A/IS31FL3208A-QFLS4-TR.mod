PCBNEW-LibModule-V1  2023-01-08 11:58:05
# encoding utf-8
Units mm
$INDEX
QFN40P400X400X80-29N-D
$EndINDEX
$MODULE QFN40P400X400X80-29N-D
Po 0 0 0 15 63baafcd 00000000 ~~
Li QFN40P400X400X80-29N-D
Cd 250-112
Kw Integrated Circuit
Sc 0
At SMD
AR 
Op 0 0 0
T0 0 0 1.27 1.27 0 0.254 N V 21 N "IC**"
T1 0 0 1.27 1.27 0 0.254 N I 21 N "QFN40P400X400X80-29N-D"
DS -2.625 -2.625 2.625 -2.625 0.05 24
DS 2.625 -2.625 2.625 2.625 0.05 24
DS 2.625 2.625 -2.625 2.625 0.05 24
DS -2.625 2.625 -2.625 -2.625 0.05 24
DS -2 -2 2 -2 0.1 24
DS 2 -2 2 2 0.1 24
DS 2 2 -2 2 0.1 24
DS -2 2 -2 -2 0.1 24
DS -2 -1.6 -1.6 -2 0.1 24
DC -2.375 -1.8 -2.275 -1.8 0.254 21
$PAD
Po -1.95 -1.2
Sh "1" R 0.2 0.85 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -1.95 -0.8
Sh "2" R 0.2 0.85 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -1.95 -0.4
Sh "3" R 0.2 0.85 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -1.95 0
Sh "4" R 0.2 0.85 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -1.95 0.4
Sh "5" R 0.2 0.85 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -1.95 0.8
Sh "6" R 0.2 0.85 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -1.95 1.2
Sh "7" R 0.2 0.85 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -1.2 1.95
Sh "8" R 0.2 0.85 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -0.8 1.95
Sh "9" R 0.2 0.85 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -0.4 1.95
Sh "10" R 0.2 0.85 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 0 1.95
Sh "11" R 0.2 0.85 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 0.4 1.95
Sh "12" R 0.2 0.85 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 0.8 1.95
Sh "13" R 0.2 0.85 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 1.2 1.95
Sh "14" R 0.2 0.85 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 1.95 1.2
Sh "15" R 0.2 0.85 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 1.95 0.8
Sh "16" R 0.2 0.85 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 1.95 0.4
Sh "17" R 0.2 0.85 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 1.95 0
Sh "18" R 0.2 0.85 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 1.95 -0.4
Sh "19" R 0.2 0.85 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 1.95 -0.8
Sh "20" R 0.2 0.85 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 1.95 -1.2
Sh "21" R 0.2 0.85 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 1.2 -1.95
Sh "22" R 0.2 0.85 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 0.8 -1.95
Sh "23" R 0.2 0.85 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 0.4 -1.95
Sh "24" R 0.2 0.85 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 0 -1.95
Sh "25" R 0.2 0.85 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -0.4 -1.95
Sh "26" R 0.2 0.85 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -0.8 -1.95
Sh "27" R 0.2 0.85 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po -1.2 -1.95
Sh "28" R 0.2 0.85 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 0 0
Sh "29" R 2.5 2.5 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$EndMODULE QFN40P400X400X80-29N-D
$EndLIBRARY
